# Copyright salsa-ci-team and others
# SPDX-License-Identifier: FSFAP
# Copying and distribution of this file, with or without modification, are
# permitted in any medium without royalty provided the copyright notice and
# this notice are preserved. This file is offered as-is, without any warranty.

FROM registry.gitlab.com/awaittrot1/docker-images/mobian-builder-amd64:latest

## Add GBP dependancies
# Add deb-src entries
RUN \
    sed -n '/^deb\s/s//deb-src /p' /etc/apt/sources.list > /etc/apt/sources.list.d/deb-src.list

RUN eatmydata apt-get install --no-install-recommends -y \
    git-buildpackage \
    devscripts \
    pristine-tar \
    curl \
    aptitude

# Just for backward compatibility, because current pipeline does gbp pull for no reasons... Remove it after merge
RUN eatmydata apt-get install ca-certificates fakeroot --no-install-recommends -y

# Support for docker:dind
ENV DOCKER_HOST=tcp://docker:2375

## Add autopkgtest dependancies

RUN eatmydata apt-get install -y \
        dnsmasq-base debci libvirt-clients libvirt-daemon-system libvirt-daemon-system-sysv \
        autopkgtest lxc wget && \
    rm /usr/sbin/policy-rc.d && \
    ln -s /bin/true /usr/sbin/policy-rc.d

ADD https://salsa.debian.org/salsa-ci-team/pipeline/-/raw/master/images/files/autopkgtest/lxc-net /etc/default/lxc-net
ADD https://salsa.debian.org/salsa-ci-team/pipeline/-/raw/master/images/files/autopkgtest/default.conf /etc/lxc/default.conf
ADD https://salsa.debian.org/salsa-ci-team/pipeline/-/raw/master/images/files/autopkgtest/fstab /etc/fstab

ADD https://salsa.debian.org/salsa-ci-team/pipeline/-/raw/master/images/patches/autopkgtest/debci-localtest.patch /tmp/debci-localtest.patch
ADD https://salsa.debian.org/salsa-ci-team/pipeline/-/raw/master/images/patches/autopkgtest/environment.sh.patch /tmp/environment.sh.patch

RUN patch /usr/share/debci/bin/debci-localtest /tmp/debci-localtest.patch
RUN patch /usr/share/debci/lib/environment.sh /tmp/environment.sh.patch

RUN useradd --gid 100 -m salsa-ci

## BLHC dependancies
RUN eatmydata apt-get install blhc -y

## Lintian Dependancies
RUN \
set -eux; \
apt-get update; \
printf 'Package: *\nPin: release a=%s\nPin-Priority: 900\n' "$RELEASE" \
  | tee /etc/apt/preferences.d/salsa-ci; \
eatmydata apt-get -y install git lintian; \
if [ -z "$(apt-cache search --names-only ^python3-junit\\.xml\$)" ]; then \
    eatmydata apt-get -y install python3-pip; \
    pip3 install junit-xml; \
else \
    eatmydata apt-get -y install python3-junit.xml; \
fi; \
:
ADD https://salsa.debian.org/salsa-ci-team/pipeline/-/raw/master/images/scripts/lintian2junit.py /usr/local/bin/lintian2junit.py
RUN chmod 755 /usr/local/bin/lintian2junit.py

## Piuparts dependancies
# piuparts also needs docker, but installing it is failing
RUN eatmydata apt-get install piuparts local-apt-repository --no-install-recommends -y
ADD https://salsa.debian.org/salsa-ci-team/pipeline/-/raw/master/images/scripts/post_install_remove_configs /etc/piuparts/scripts/post_install_remove_configs
ADD https://salsa.debian.org/salsa-ci-team/pipeline/-/raw/master/images/scripts/pre_install_copy_configs /etc/piuparts/scripts/pre_install_copy_configs
RUN chmod -c o-w /etc/piuparts/scripts/*_install_*_configs

## Reprotest
RUN eatmydata apt-get install reprotest patch faketime locales-all \
 disorderfs sudo xxd unzip --no-install-recommends -y && rm -rf /var/lib/apt

ADD https://salsa.debian.org/salsa-ci-team/pipeline/-/raw/master/images/patches/reprotest/build.patch /tmp/build.patch
RUN patch /usr/lib/python3/dist-packages/reprotest/build.py /tmp/build.patch
ENV PYTHONIOENCODING utf-8
